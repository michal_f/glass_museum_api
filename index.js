const port = 3000;

const express = require ('express')
const cors = require ('cors')
const server = require('http')

const app = express()
app.use(express.json())
app.use(cors())

app.listen(port, () => console.log(`I am listening at the port ${port}, Dave`))